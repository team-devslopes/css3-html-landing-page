# Slope Session - CSS3 + HTML: Create a Video or Image Landing Page
This is the source code for a CSS/HTML tutorial that shows how to create a video or image landing page.

## Part 1 - Starting Out
Make sure you use the **part-1-start** branch when starting the tutorial.

## Part 1 - Complete
The **part-1-complete** branch is a copy of my code when I completed the tutorial video.

## Part 2 - Starting Out
Make sure you use the **part-1-complete** branch when starting the tutorial for part 2 (or use your own end result from part 1).

## Part 2 - Complete
The **master** branch is a copy of my code when I completed part 2 of the tutorial.

Thanks

Jess Rascal